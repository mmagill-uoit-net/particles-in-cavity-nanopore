import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# Select case
rante = float(sys.argv[1])
zante = float(sys.argv[2])
rmax = rante
zmax = zante 
Nr = int(sys.argv[3])
Nz = int(sys.argv[4])

# Load data
u = np.loadtxt('data/soln_%.1f_%.1f_%03d_%03d.dat'%
               (rante,zante,Nr,Nz))
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)

# Reshape and output as a table for Espresso loader
rprint = (R.reshape((Nr*Nz))/dr).astype(int)
zprint = (Z.reshape((Nr*Nz))/dz).astype(int)
Erprint = Er.reshape((Nr*Nz))
Ezprint = Ez.reshape((Nr*Nz))
for k in range(Nr*Nz):
    print "%d %d %e %e"%(rprint[k],zprint[k],Erprint[k],Ezprint[k])
