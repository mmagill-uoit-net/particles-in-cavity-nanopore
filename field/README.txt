

Antechamber
	Diameter ~ 1000 nm
	Radius 	 ~  500 nm
	Height	 ~  200 nm

Pore
	Diameter ~ 5-15 nm
	Radius 	 ~ 2.5-7.5 nm



For simplest model, choose 

    dr =    2.5 nm
    rpore = 2 bins  = 5 nm
    rante = 128-256 = 320-640 nm
    Nr	  = 128-256    

    Nz    = 64
    zante = 256 nm
    dz    = 4 nm

Runtime
	small	 0m37.029s
	big	 5m36.748s
